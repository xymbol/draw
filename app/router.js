import Ember from 'ember';

var Router = Ember.Router.extend({
  location: DrawENV.locationType
});

Router.map(function() {
  this.route('names');
});

export default Router;
