import Ember from 'ember';
import Name from '../models/name';

export default Ember.ArrayController.extend({
  actions: {
    createName: function() {
      this.get('parsedNames').forEach(function(name) {
        this.addObject(Name.create({ name: name }));
      }, this);
      this.set('newName', '');
    },

    pickWinner: function() {
      var winner = Math.floor(Math.random() * this.get('length'));
      this.forEach(function(name, index) {
        name.set('isWinner', winner === index);
      });
    },

    clearNames: function() {
      this.clear();
    }
  },

  parsedNames: function() {
    return this.get('newName').split(',').map(function(name) {
      return name.trim();
    }).filter(function(name) {
      return !!name;
    });
  }.property('newName')
});
